﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class AutoPopulateFolders : MonoBehaviour
{

    [MenuItem("Project Tools/Auto-populate Folders #&g")]

    ///<summary>
    ///Automatically creates a basic folder hierarchy.
    /// </summary>
    public static void CreateFolder()
    {
        //Start creating folders
        AssetDatabase.CreateFolder("Assets", "Dynamic Assets");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets", "Resources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animations");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Animation Controllers");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Effects");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Models");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Models", "Environment");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Prefabs", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Sounds");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Sounds/SFX", "Common");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources", "Textures");
        AssetDatabase.CreateFolder("Assets/Dynamic Assets/Resources/Textures", "Common");

        //Create a .txt file with instructions on what goes in the folder/how to use it
        System.IO.File.WriteAllText(Application.dataPath + "/Dynamic Assets/Resources/folderStructure.txt", "All dynamic" +
                                                           " assets (placed into the game DURING runtime) go here.\n" +
                                                           "For the Animation folder, create a new folder that shares " +
                                                           "the name of the object you're attempting to import in both " +
                                                           "the Animations and Animations/Sources folders.\nImport the " +
                                                           "object to the Animations/Sources/object folder you created, " +
                                                           "then copy the nested .anim files over to the Animations/object " +
                                                           "folder.\nLastly, create an animation controller named after " +
                                                           "the imported object, and place it in the AnimationControllers folder.");

        //Rinse and repeat for a variety of folders
        AssetDatabase.CreateFolder("Assets", "Extentions");
        System.IO.File.WriteAllText(Application.dataPath + "/Extentions/folderStructure.txt", "Third-party asset addons or extensions.");

        AssetDatabase.CreateFolder("Assets", "Gizmos");
        System.IO.File.WriteAllText(Application.dataPath + "/Gizmos/folderStructure.txt", "Scripts for creating or controlling Unity gizmos in the editor");

        AssetDatabase.CreateFolder("Assets", "Plugins");
        System.IO.File.WriteAllText(Application.dataPath + "/Plugins/folderStructure.txt", "Plugin scripts");

        AssetDatabase.CreateFolder("Assets", "Scripts");
        AssetDatabase.CreateFolder("Assets/Scripts", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "Scripts sorted by level/type. Scripts found in multiple levels go in Common.");

        AssetDatabase.CreateFolder("Assets", "Shaders");
        System.IO.File.WriteAllText(Application.dataPath + "/Shaders/folderStructure.txt", "Shaders and shader scripts");

        AssetDatabase.CreateFolder("Assets", "Static Assets");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animations");
        AssetDatabase.CreateFolder("Assets/Static Assets/Animations", "Sources");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Animation Controllers");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Effects");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Models");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Character");
        AssetDatabase.CreateFolder("Assets/Static Assets/Models", "Environment");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Static Assets/Prefabs", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Scenes");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Sounds");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "Music");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/Music", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds", "SFX");
        AssetDatabase.CreateFolder("Assets/Static Assets/Sounds/SFX", "Common");
        AssetDatabase.CreateFolder("Assets/Static Assets", "Textures");
        AssetDatabase.CreateFolder("Assets/Static Assets/Textures", "Common");
        System.IO.File.WriteAllText(Application.dataPath + "/Static Assets/folderStructure.txt", "All STATIC (non-changing) assets go here");

        AssetDatabase.CreateFolder("Assets", "Testing");

        //Finally, refresh Unity's local version of the hierarchy to reflect the changes we just made
        AssetDatabase.Refresh();
    }
}
