﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
  public int numLives = 3;
  public float currentHealth;
  private float maxHealth = 100f;
  
  void Start ()
  {
    if(numLives < 3 || numLives > 3)
    {
      numLives = 3;
    }
    currentHealth = maxHealth;
  }

  void Update()
  {
    //This method call is for testing purposes only!!!!
    //Please remove before final build!!!
    TakeLife();
  }

  public void TakeDamage(float damage)
  {
    currentHealth -= damage;
    if (currentHealth <= 0)
    {
      numLives--;
      if(numLives == 0)
      {
        PlayerDeath();
      }
      else
      {
        currentHealth = maxHealth;
      }
    }
  }

  void PlayerDeath()
  {
    string sceneName = SceneManager.GetActiveScene().name;
    SceneManager.LoadScene(sceneName);
    Time.timeScale = 1;
    AudioListener.volume = 1;
  }

  //Testing purposes only! Remove before final build!!
  void TakeLife()
  {
    if(Input.GetKeyDown(KeyCode.P))
    {
      TakeDamage(50f);
    }
  }
}
