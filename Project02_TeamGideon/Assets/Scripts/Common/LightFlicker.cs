﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightFlicker : MonoBehaviour
{
  public float timeOn = 0.1f;
  public float timeOff = 0.5f;

  private float changeTime = 0;
  private Light flickeringLight;

  void Start ()
  {
    flickeringLight = GetComponent<Light>();
  }

	// Update is called once per frame
	void Update ()
  {
		if(Time.time > changeTime)
    {
      flickeringLight.enabled = !flickeringLight.enabled;
      if(flickeringLight.enabled)
      {
        changeTime = Time.time + timeOn;
      }
      else
      {
        changeTime = Time.time + timeOff;
      }
    }
	}
}
