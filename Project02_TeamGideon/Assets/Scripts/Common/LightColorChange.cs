﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightColorChange : MonoBehaviour
{
  public float lightOutDuration = 3f; 

  IEnumerator LightChangeDelay()
  {
    yield return new WaitForSeconds(lightOutDuration);
    gameObject.GetComponent<Light>().color = Color.red;
    gameObject.GetComponent<Light>().intensity = 2;
    gameObject.GetComponent<Light>().enabled = true;
  }

  void OnTriggerEnter (Collider other)
  {
    if(other.transform.CompareTag("Player") == true)
    {
      gameObject.GetComponent<Light>().enabled = false;
      Destroy(gameObject.GetComponent<LightFlicker>());
      StartCoroutine(LightChangeDelay());
      Destroy(gameObject.GetComponent<BoxCollider>());
    }
  }
}
