﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Camera-Control/Smooth Mouse Look")]
public class PlayerMove : MonoBehaviour
{
    public float walkSpeed = 6f;
    public float jumpSpeed = 4f;
    public float runSpeed = 12f;
    public float gravity = 9.8f;
    public bool useGravity = true;

    private Vector3 moveDirection = Vector3.zero;
    private CharacterController charController;
    private bool isCrouch = false;
    private Transform playerObj;

    // Use this for initialization
    void Start()
    {
        //Get the CharacterController component from this object
        charController = GetComponent<CharacterController>();
        playerObj = GetComponent<Transform>();
        LockCursor();
    }

    // Update is called once per frame
    void Update()
    {
        //Is this character controller grounded?
        if (charController.isGrounded == true)
        {
            //Set up moveDirection with input from the Horizontal (x) and Vertical axis (z)
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            //Change the axis to local coordinates
            moveDirection = transform.TransformDirection(moveDirection);

            //Apply speed
            moveDirection *= GetSpeed();

            //Pressing jump key?
            if (Input.GetButtonDown("Jump"))
            {
                //Add jumpSpeed to moveDirection.y
                moveDirection.y += jumpSpeed;
            }

        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            transform.position = GameObject.FindWithTag("DevRespawn").transform.position;
        }

        //Is the char using gravity?
        if (useGravity)
        {
            //Subtract gravity from moveDirection.y, accounting for frame-rate independence
            moveDirection.y -= (gravity * Time.deltaTime);
        }

        //call Move method on the character controller -- CharacterControllerMove()
        charController.Move(moveDirection * Time.deltaTime);

        //Press Escape?
        if (Input.GetKeyDown(KeyCode.P))
        {
            LockCursor();
        }
        
        //Player pressing C enter crouch
        if (Input.GetKeyDown(KeyCode.C))
        {
          isCrouch = !isCrouch;
          Crouch();
        }

        //Player released the C key, exit crouch.
        if(Input.GetKeyUp(KeyCode.C))
        {
          isCrouch = false;
          Crouch();
        }
    }

    float GetSpeed()
    {
        //Player pressing LeftShit?
        if (Input.GetKey(KeyCode.LeftShift))
        {
            //return runSpeed
            return runSpeed;
        }
        //return runSpeed
        else
        {
            return walkSpeed;
        }
    }

    void LockCursor()
    {
        //is cursor currently locked?
        if (Cursor.lockState == CursorLockMode.Locked)
        {
            //Set it to not be locked
            Cursor.lockState = CursorLockMode.None;
        }
        //else
        else
        {
            //set it to be locked
            Cursor.lockState = CursorLockMode.Locked;
        }

        //toggle cursor visibility
        Cursor.visible = !Cursor.visible;
    }

    /// <summary>
    /// Changes the player's scale to allow them to enter a crouch state
    /// or to revert from a crouch state to a normal standing position.
    /// </summary>
    void Crouch()
    {
      if(isCrouch == true)
      {
        playerObj.localScale = new Vector3(1, 0.5f, 1);
        charController.radius = 0.5f;
        charController.height = 1f;
      }
      else
      {
        playerObj.localScale = new Vector3(1, 1, 1);
        charController.radius = 0.5f;
        charController.height = 2f;
      }
    }
}
