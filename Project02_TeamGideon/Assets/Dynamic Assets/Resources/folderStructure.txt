All dynamic assets (placed into the game DURING runtime) go here.
For the Animation folder, create a new folder that shares the name of the object you're attempting to import in both the Animations and Animations/Sources folders.
Import the object to the Animations/Sources/object folder you created, then copy the nested .anim files over to the Animations/object folder.
Lastly, create an animation controller named after the imported object, and place it in the AnimationControllers folder.