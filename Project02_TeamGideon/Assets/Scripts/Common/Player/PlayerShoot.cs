﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour
{
  public float damage = 15f;
  public float range = 100f;
  public float fireRate = 1f;
  public float maxMagAmmoAmount = 30f;
  public float currentMagAmount;
  public float maxAmmo = 100f;
  public float weaponReloadSpeed = 3f;
  public Camera fpsCamera;
  public ParticleSystem muzzleFlash;

  private bool isReloading = false;
  private float nextTimeToFire = 0f;

  void Start ()
  {
    currentMagAmount = maxMagAmmoAmount;
  }

	// Update is called once per frame
	void Update ()
  {
    //Player presses Mouse Button 1 and enough time has elapsed since last shot.
    if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire && currentMagAmount > 0 && isReloading == false)
    {
      currentMagAmount--;
      nextTimeToFire = Time.time + fireRate;
      Shoot();
    }

    Reload();
	}

  /// <summary>
  /// Shoot sends out a raycast to check if the target it hits is an enemy
  /// and if it is an enemy, it locates the enemies health and subtracts 
  /// the weapons damage from the enemies health.
  /// </summary>
  void Shoot ()
  {
    muzzleFlash.Play();
    RaycastHit hit;
    if(Physics.Raycast(fpsCamera.transform.position, fpsCamera.transform.forward, out hit, range));
    {
      Debug.Log(hit.transform.name);

      Enemy enemy = hit.transform.GetComponent<Enemy>();
      if(enemy != null)
      {
        enemy.TakeDamage(damage);
      }

      if(hit.transform.CompareTag("Destructable"))
      {
        hit.transform.GetComponent<EnvironmentDestructable>().TakeDamage(damage);
      }
    }
  }

  /// <summary>
  /// Reloads the current weapons magazine unless the magazine is at max
  /// Capacity.
  /// </summary>
  void Reload ()
  {
    if (Input.GetKeyDown(KeyCode.R))
    {
      if (currentMagAmount < 30 && isReloading == false)
      {
        isReloading = true;
        StartCoroutine(ReloadTime());
      }
    }
  }

  IEnumerator ReloadTime()
  {
    yield return new WaitForSeconds(weaponReloadSpeed);
    float currentMagDifference = maxMagAmmoAmount - currentMagAmount;
    maxAmmo -= currentMagDifference;
    currentMagAmount = maxMagAmmoAmount;
    isReloading = false;
  }

}
