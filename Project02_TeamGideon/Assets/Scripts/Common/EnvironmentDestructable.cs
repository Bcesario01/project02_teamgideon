﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentDestructable : MonoBehaviour
{
  public float itemHealth = 5f;

  void Start()
  {
    if(itemHealth > 5 || itemHealth < 1)
    {
      itemHealth = 3f;
    }
  }

  public void TakeDamage(float damage)
  {
    itemHealth -= damage;
    if(itemHealth <= 0)
    {
      Destroy(gameObject);
    }
  }
}
